// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "GAS/Ability/GameplayAbilityInfo.h"
#include "GameplayAbilityBase.generated.h"

/**
 * 
 */
UCLASS()
class GAS_API UGameplayAbilityBase : public UGameplayAbility
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, Category = "AbilityBase")
	UMaterialInstance* IconMaterial;


	UFUNCTION(BlueprintCallable, Category = "AbilityBase")
	FGameplayAbilityInfo GetAbilityInfo() const;
	
	virtual bool CommitAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, FGameplayTagContainer* OptionalRelevantTags) override;

	UFUNCTION(BlueprintCallable)
	void SetSlotNumber(int32 InSlotNumber);

	UFUNCTION(BlueprintPure)
	int32 GetSlotNumber();
	
	UFUNCTION(BlueprintCallable, Category = "AbilityTag")
	void AddCancelAbilityTag(FGameplayTag& Tag);
protected:
	UPROPERTY(EditDefaultsOnly, Category = "SlotNumber", meta=(AllowPrivateAccess=true))
	int32 SlotNumber;
};
