// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemInterface.h"
#include "Abilities/GameplayAbility.h"
#include "GameFramework/Character.h"
#include "GAS/Ability/AttributeSetBase.h"
#include "CharacterBase.generated.h"

class UGameplayAbilityBase;

UENUM(BlueprintType)
enum  class ETEAM : uint8
{
	TEAM_A = 0,
	TEAM_B = 1,
	TEAM_C = 2
};

UCLASS()
class GAS_API ACharacterBase : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacterBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	
	UFUNCTION(BlueprintCallable, Category = "AbilitySystem")
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UFUNCTION(BlueprintCallable, Category = "AbilitySystem")
	void AquireAbility(TSubclassOf<UGameplayAbility> AbilityToAcquire);

	UFUNCTION(BlueprintCallable, Category = "AbilitySystem")
	void AquireAbilities(TArray<TSubclassOf<UGameplayAbility>> Abilities);
	
	UFUNCTION()
	void OnHealthChanged(float Health, float MaxHealth);

	UFUNCTION(BlueprintImplementableEvent, Category = "AttributeSet", meta=(DisplayName = "OnHealthChanged"))
	void K2_OnHealthChanged(float Health, float MaxHealth);
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Death", meta=(DisplayName = "Die"))
	void K2_Die();
	
	UFUNCTION(BlueprintCallable)
	void BeforeDeath();

	UFUNCTION()
	void OnMannaChanged(float Manna, float MaxManna);

	UFUNCTION(BlueprintImplementableEvent, Category = "AttributeSet",meta=(DisplayName = "OnMannaChanged"))
	void K2_OnMannaChanged(float Manna, float MaxManna);

	UFUNCTION()
	void OnStrengthChanged(float Strength, float MaxStrength);

	UFUNCTION(BlueprintImplementableEvent, Category = "AttributeSet",meta=(DisplayName = "OnStrengthChanged"))
	void K2_OnStrengthChanged(float Strength, float MaxStrength);
	
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE bool IsSameTeam(ACharacterBase* Other) const {return Team_ID == Other->GetTeam();}

	FORCEINLINE ETEAM GetTeam() const {return Team_ID;}

	UFUNCTION(BlueprintCallable, Category = "CharacterTag")
	void AddGameplayTag(FGameplayTag& AddTag);

	UFUNCTION(BlueprintCallable, Category = "CharacterTag")
	void RemoveGameplayTag(FGameplayTag& RemoveTag);

	UPROPERTY(EditAnywhere, BlueprintReadWrite ,Category = "GameplayTarget")
	FGameplayTag FullHealthTag;

	UFUNCTION(BlueprintCallable, Category = "Stun")
	void StunMoment(float TimeToStun);
protected:
	UAbilitySystemComponent* AbilitySystemComp;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category = "AttributeSet", meta=(AllowPrivateAccess=true))
	UAttributeSetBase* AttributeSetComp;

	bool bIsDead;

	UPROPERTY(EditDefaultsOnly, Category = "TEAM", meta=(AllowPrivateAccess=true))
	ETEAM Team_ID;

	UFUNCTION(BlueprintCallable)
	void DisableCharacterInput();

	UFUNCTION(BlueprintCallable)
	void EnableCharacterInput();

	void AddAbilityToUI(TSubclassOf<UGameplayAbilityBase> Ability);
	
	FTimerHandle StunTimerHandle;
};
