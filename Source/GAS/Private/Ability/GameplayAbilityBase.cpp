// Fill out your copyright notice in the Description page of Project Settings.


#include "Ability/GameplayAbilityBase.h"

FGameplayAbilityInfo UGameplayAbilityBase::GetAbilityInfo() const
{
	UGameplayEffect* CooldownEffect = GetCooldownGameplayEffect();
	UGameplayEffect* CostEffect = GetCostGameplayEffect();

	if (CooldownEffect && CostEffect) {
		float CoolDuration = 0, Cost = 0;
		ECostType CostType = ECostType::ECT_Default;
		
		CooldownEffect->DurationMagnitude.GetStaticMagnitudeIfPossible(1, CoolDuration);
		if (CostEffect->Modifiers.Num() > 0) {
			FGameplayModifierInfo CostModifier = CostEffect->Modifiers[0];
			CostModifier.ModifierMagnitude.GetStaticMagnitudeIfPossible(1, Cost);

			FGameplayAttribute CostAttribute = CostModifier.Attribute;
			FString PropertyName = CostAttribute.AttributeName;
			if (PropertyName == "Health") {
				CostType = ECostType::ECT_Health;		
			} else if (PropertyName == "Manna") {
				CostType = ECostType::ECT_Manna;
			} else if (PropertyName == "Strength") {
				CostType = ECostType::ECT_Strength;
			}

			return FGameplayAbilityInfo(CoolDuration, Cost, CostType, IconMaterial, GetClass());
		}
		
	}
	return FGameplayAbilityInfo();
}

bool UGameplayAbilityBase::CommitAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, FGameplayTagContainer* OptionalRelevantTags)
{
	return Super::CommitAbility(Handle, ActorInfo, ActivationInfo, OptionalRelevantTags);
}

void UGameplayAbilityBase::SetSlotNumber(int32 InSlotNumber)
{
	SlotNumber = InSlotNumber;
}

int32 UGameplayAbilityBase::GetSlotNumber()
{
	return SlotNumber;
}

void UGameplayAbilityBase::AddCancelAbilityTag(FGameplayTag& Tag)
{
	CancelAbilitiesWithTag.AddTag(Tag);
}
