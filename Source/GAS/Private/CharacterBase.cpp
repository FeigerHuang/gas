// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterBase.h"
#include "AbilitySystemComponent.h"
#include "AIController.h"
#include "BrainComponent.h"
#include "PlayerControllerBase.h"
#include "Ability/GameplayAbilityBase.h"

// Sets default values
ACharacterBase::ACharacterBase()
	: Team_ID(ETEAM::TEAM_A)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bIsDead = false;
	
	AbilitySystemComp = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystemComp"));
	AttributeSetComp = CreateDefaultSubobject<UAttributeSetBase>(TEXT("AttributeSetComp"));

}

// Called when the game starts or when spawned
void ACharacterBase::BeginPlay()
{
	Super::BeginPlay();

	AttributeSetComp->OnHealthChange.AddDynamic(this, &ACharacterBase::OnHealthChanged);
	AttributeSetComp->OnMannaChange.AddDynamic(this,&ACharacterBase::OnMannaChanged);
	AttributeSetComp->OnStrengthChange.AddDynamic(this,&ACharacterBase::OnStrengthChanged);
	AddGameplayTag(FullHealthTag);
}

// Called every frame
void ACharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UAbilitySystemComponent* ACharacterBase::GetAbilitySystemComponent() const
{
	return AbilitySystemComp;
}

void ACharacterBase::AquireAbility(TSubclassOf<UGameplayAbility> AbilityToAcquire)
{
	if (HasAuthority() && AbilityToAcquire) {
		AbilitySystemComp->GiveAbility(FGameplayAbilitySpec(AbilityToAcquire, 1, 0));
	}
	AbilitySystemComp->InitAbilityActorInfo(this, this);
}

void ACharacterBase::AquireAbilities(TArray<TSubclassOf<UGameplayAbility>> Abilities)
{
	for (auto Ability : Abilities) {
		AquireAbility(Ability);
		if (Ability->IsChildOf(UGameplayAbilityBase::StaticClass()) ) {
			TSubclassOf<UGameplayAbilityBase> AbilityBase = *Ability;
			if (AbilityBase != nullptr) {
				AddAbilityToUI(AbilityBase);
			}
		}
	}
}

void ACharacterBase::OnHealthChanged(float Health, float MaxHealth)
{
	if (FMath::IsNearlyZero(Health, 0.1f) && !bIsDead) {
		bIsDead = true;
		BeforeDeath();
		K2_Die();
	}
	K2_OnHealthChanged(Health, MaxHealth);
}

void ACharacterBase::BeforeDeath()
{
	DisableCharacterInput();
}

void ACharacterBase::OnMannaChanged(float Manna, float MaxManna)
{
	K2_OnMannaChanged(Manna, MaxManna);
}

void ACharacterBase::OnStrengthChanged(float Strength, float MaxStrength)
{
	K2_OnStrengthChanged(Strength, MaxStrength);
}

void ACharacterBase::AddGameplayTag(FGameplayTag& AddTag)
{
	GetAbilitySystemComponent()->AddLooseGameplayTag(AddTag);
	GetAbilitySystemComponent()->SetLooseGameplayTagCount(AddTag, 1);
}

void ACharacterBase::RemoveGameplayTag(FGameplayTag& RemoveTag)
{
	GetAbilitySystemComponent()->RemoveLooseGameplayTag(RemoveTag);
}

void ACharacterBase::StunMoment(float TimeToStun)
{
	DisableCharacterInput();
	GetWorldTimerManager().SetTimer(StunTimerHandle, this, &ACharacterBase::EnableCharacterInput, TimeToStun, false);
}

void ACharacterBase::DisableCharacterInput()
{
	if (!bIsDead) {
		APlayerController* PC = Cast<APlayerController>(GetController());
		if (PC) {
			PC->DisableInput(PC);
		}
		AAIController* AIC = Cast<AAIController>(GetController());
		if (AIC) {
			AIC->GetBrainComponent()->StopLogic("Pause");
		}
	}
}
void ACharacterBase::EnableCharacterInput()
{
	if (!bIsDead) {
		APlayerController* PC = Cast<APlayerController>(GetController());
		if (PC) {
			PC->EnableInput(PC);
		}
		AAIController* AIC = Cast<AAIController>(GetController());
		if (AIC) {
			AIC->GetBrainComponent()->RestartLogic();
		}
	}
}

void ACharacterBase::AddAbilityToUI(TSubclassOf<UGameplayAbilityBase> Ability)
{
	APlayerControllerBase*  PC = Cast<APlayerControllerBase>(GetController());

	if (PC) {
		UGameplayAbilityBase* AbilityIns = Ability.Get()->GetDefaultObject<UGameplayAbilityBase>();
		if (AbilityIns) {
			FGameplayAbilityInfo AbilityInfo = AbilityIns->GetAbilityInfo();
			PC->AddAbilityToUI(AbilityInfo);
		}
	}
}


