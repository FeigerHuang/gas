// Fill out your copyright notice in the Description page of Project Settings.


#include "GATargetActorGroundSelect.h"
#include "DrawDebugHelpers.h"
#include "Abilities/GameplayAbility.h"
#include "Components/DecalComponent.h"

AGATargetActorGroundSelect::AGATargetActorGroundSelect()
{
	PrimaryActorTick.bCanEverTick = true;

	RootComp = CreateDefaultSubobject<USceneComponent>(TEXT("RootComp"));
	DecalComp = CreateDefaultSubobject<UDecalComponent>(TEXT("DecaleComp"));

	SetRootComponent(RootComp);
	DecalComp->SetupAttachment(RootComp);
	Radius = 200.f;
	DecalComp->DecalSize = FVector(Radius);
}

void AGATargetActorGroundSelect::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
	FVector LookPoint;
	GetPlayerLookingPoint(LookPoint);
	//DrawDebugSphere(GetWorld(), LookPoint, Radius, 32, FColor::Red, false, -1.f, 0, 5.0f);
	DecalComp->SetWorldLocation(LookPoint);
}

void AGATargetActorGroundSelect::StartTargeting(UGameplayAbility* Ability)
{
	OwningAbility = Ability;
	// Get Ability Owner's Controller;
	MasterPC = Cast<APlayerController>(Ability->GetOwningActorFromActorInfo()->GetInstigatorController());

	DecalComp->DecalSize = FVector(Radius);
}

// when player click this will confirmed
void AGATargetActorGroundSelect::ConfirmTargetingAndContinue()
{
	FVector ViewLocation;
	GetPlayerLookingPoint(ViewLocation);

	TArray<FOverlapResult> Overlaps;
	TArray<TWeakObjectPtr<AActor>> OverlapedActors;
	bool TraceComplex = false;

	// set Query params that we need to overlap
	FCollisionQueryParams CollisionQueryParams;
	CollisionQueryParams.bTraceComplex = TraceComplex;
	CollisionQueryParams.bReturnPhysicalMaterial = false;
	APawn* MasterPawn = MasterPC->GetPawn();
	if (MasterPawn) {
		CollisionQueryParams.AddIgnoredActor(MasterPawn->GetUniqueID());
	}

	// trying to see what pawn our overlapping with;
	bool TryOverlap = GetWorld()->OverlapMultiByObjectType(
		Overlaps,
		ViewLocation,
		FQuat::Identity,
		FCollisionObjectQueryParams(ECC_Pawn),
		FCollisionShape::MakeSphere(Radius),
		CollisionQueryParams);

	// is the overlaped pawn not in Array, add it;
	if (TryOverlap) {
		for (int32 i = 0; i < Overlaps.Num(); ++i) {
			APawn* PawnOverlaped = Cast<APawn>(Overlaps[i].GetActor());
			if (PawnOverlaped && !OverlapedActors.Contains(PawnOverlaped)) {
				OverlapedActors.Add(PawnOverlaped);
			}
		}
	}

	/** Note: This is cleaned up by the FGameplayAbilityTargetDataHandle (via an internal TSharedPtr) */
	FGameplayAbilityTargetData_LocationInfo* BlastLocation = new FGameplayAbilityTargetData_LocationInfo;
	if (DecalComp) {
		BlastLocation->TargetLocation.LocationType = EGameplayAbilityTargetingLocationType::LiteralTransform;
		BlastLocation->TargetLocation.LiteralTransform = DecalComp->GetComponentTransform();
	}
	
	FGameplayAbilityTargetDataHandle TargetData;
	TargetData.Add(BlastLocation);
	
	if (OverlapedActors.Num() > 0) {
		// this will create a target data that we can broadcast out;
		FGameplayAbilityTargetDataHandle TargetDataEnemy = StartLocation.MakeTargetDataHandleFromActors(OverlapedActors);
		TargetData.Append(TargetDataEnemy);
	}
	
	// do broadcast
	TargetDataReadyDelegate.Broadcast(TargetData);
}

void AGATargetActorGroundSelect::ConfirmTargeting()
{
	Super::ConfirmTargeting();
}

bool AGATargetActorGroundSelect::GetPlayerLookingPoint(FVector& OutViewPoint)
{
	// Where the player is looking at;
	FVector ViewPoint;
	FRotator ViewRotation;
	MasterPC->GetPlayerViewPoint(ViewPoint, ViewRotation);

	FHitResult HitResult;
	FCollisionQueryParams QueryParams;
	QueryParams.bTraceComplex = true;

	APawn* MasterPawn = MasterPC->GetPawn();
	if (MasterPawn) {
		QueryParams.AddIgnoredActor(MasterPawn->GetUniqueID());
	}

	bool TryTrace = GetWorld()->LineTraceSingleByChannel(HitResult, ViewPoint, ViewPoint + ViewRotation.Vector() * 10000.f, ECC_Visibility, QueryParams);
	if (TryTrace) {
		// where is the player looking
		OutViewPoint = HitResult.ImpactPoint;
	} else {
		OutViewPoint = FVector();
	}
	return TryTrace;
}

