// Fill out your copyright notice in the Description page of Project Settings.


#include "GATargetActorAround.h"
#include "Abilities/GameplayAbility.h"

AGATargetActorAround::AGATargetActorAround() {}

void AGATargetActorAround::StartTargeting(UGameplayAbility* Ability)
{
	Super::StartTargeting(Ability);
	MasterPC = Cast<APlayerController>(OwningAbility->GetOwningActorFromActorInfo()->GetInstigatorController());
}

void AGATargetActorAround::ConfirmTargetingAndContinue()
{

	if (MasterPC == nullptr) {
		TargetDataReadyDelegate.Broadcast(FGameplayAbilityTargetDataHandle());
		return;
	}

	
	APawn* MasterPawn = MasterPC->GetPawn();
	TArray<FOverlapResult> OverlapResults;

	// init the queryParams
	bool TraceComplex = false;
	FVector QueryPos = MasterPawn->GetActorLocation();
	FCollisionQueryParams CollisionQueryParams;
	
	CollisionQueryParams.bTraceComplex = TraceComplex;
	CollisionQueryParams.bReturnPhysicalMaterial = false;
	CollisionQueryParams.AddIgnoredActor(MasterPawn->GetUniqueID());

	// Use this Api do Query;
	bool Traced = GetWorld()->OverlapMultiByObjectType(
		OverlapResults,
		QueryPos,
		FQuat::Identity,
		FCollisionObjectQueryParams(ECC_Pawn),
		FCollisionShape::MakeSphere(Radius),
		CollisionQueryParams);

	// Get the Overlap Actors from results;
	TArray<TWeakObjectPtr<AActor>> OverlapActors;
	if (OverlapResults.Num() > 0) {
		for(auto elem : OverlapResults) {
			OverlapActors.Add(elem.GetActor());
		}
	}

	// Broadcast we get what we wantted;
	if (OverlapActors.Num() > 0) {
		FGameplayAbilityTargetDataHandle TargetData = StartLocation.MakeTargetDataHandleFromActors(OverlapActors);
		TargetDataReadyDelegate.Broadcast(TargetData);
	} else {
		TargetDataReadyDelegate.Broadcast(FGameplayAbilityTargetDataHandle());
	}
	return;
}

