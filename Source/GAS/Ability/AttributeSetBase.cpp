// Fill out your copyright notice in the Description page of Project Settings.

#include "AttributeSetBase.h"

#include "CharacterBase.h"
#include "GameplayEffectExtension.h"

UAttributeSetBase::UAttributeSetBase()
	: Health(200.f)
	, MaxHealth(200.f)
{
	InitManna(120.f);
	InitMaxManna(120.f);
	InitStrength(200.f);
	InitMaxStrength(200.f);
}

void UAttributeSetBase::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	// 进行Filed检测
	if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UAttributeSetBase::StaticClass(), GET_MEMBER_NAME_CHECKED(UAttributeSetBase, Health))) {
		float ActualHealth = FMath::Clamp<float>(Health.GetCurrentValue(), 0, MaxHealth.GetCurrentValue());
		Health.SetBaseValue(ActualHealth);
		Health.SetCurrentValue(ActualHealth);
		UE_LOG(LogTemp, Warning, TEXT("After Effect damage, now my health = %f"), Health.GetCurrentValue());
		OnHealthChange.Broadcast(Health.GetCurrentValue(), MaxHealth.GetCurrentValue());

		ACharacterBase* Character = Cast<ACharacterBase>(GetOwningActor());
		check(Character);
		if (ActualHealth == GetMaxHealth()) {
			Character->AddGameplayTag(Character->FullHealthTag);
		} else {
			Character->RemoveGameplayTag(Character->FullHealthTag);
		}
	}

	if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UAttributeSetBase::StaticClass(), GET_MEMBER_NAME_CHECKED(UAttributeSetBase, Manna)))
	{
		float ActualManna = FMath::Clamp<float>(GetManna(), 0, GetMaxManna());
		SetManna(ActualManna);
		OnMannaChange.Broadcast(GetManna(), GetMaxManna());
	}

	if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UAttributeSetBase::StaticClass(), GET_MEMBER_NAME_CHECKED(UAttributeSetBase, Strength))) {
		float ActualStrength = FMath::Clamp<float>(GetStrength(), 0, GetMaxStrength());
		SetStrength(ActualStrength);
		OnStrengthChange.Broadcast(GetStrength(), GetMaxStrength());
	}
}
