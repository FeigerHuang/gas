// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "AttributeSetBase.generated.h"

#define ATTRIBUTE_ACCESSOR(ClassName, PropertyName)\
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName)\
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName)\
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)\
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName)
/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChangeDelegate, float, Health, float, MaxHealth);

UCLASS()
class GAS_API UAttributeSetBase : public UAttributeSet
{
	GENERATED_BODY()
public:
	UAttributeSetBase();

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = "Attribute")
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSOR(UAttributeSetBase, Health);
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attribute")
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSOR(UAttributeSetBase, MaxHealth);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attribute")
	FGameplayAttributeData Manna;
	ATTRIBUTE_ACCESSOR(UAttributeSetBase, Manna);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Attribute")
	FGameplayAttributeData MaxManna;
	ATTRIBUTE_ACCESSOR(UAttributeSetBase, MaxManna);

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = "Attribute")
	FGameplayAttributeData Strength;
	ATTRIBUTE_ACCESSOR(UAttributeSetBase, Strength);
	
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = "Attribute")
	FGameplayAttributeData MaxStrength;
	ATTRIBUTE_ACCESSOR(UAttributeSetBase, MaxStrength);

	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;

	FOnHealthChangeDelegate OnHealthChange;

	FOnHealthChangeDelegate OnMannaChange;

	FOnHealthChangeDelegate OnStrengthChange;
};
