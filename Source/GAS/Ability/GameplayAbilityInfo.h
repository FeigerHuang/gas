// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "GameplayAbilityInfo.generated.h"

UENUM(BlueprintType)
enum class ECostType : uint8
{
	ECT_Default = 0,
	ECT_Health,
	ECT_Manna ,
	ECT_Strength
};

/**
 * 
 */
USTRUCT(BlueprintType)
struct FGameplayAbilityInfo
{
	GENERATED_BODY();
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite ,Category = "AbilityInfo")
	float CoolDuration;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
	float Cost;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
	ECostType CostType;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
	UMaterialInstance* IconMat;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
	TSubclassOf<UGameplayAbility> AbilityClass;

	FGameplayAbilityInfo();

	FGameplayAbilityInfo(float InCoolDuration, float InCost, ECostType InCostType, UMaterialInstance* InIconMat, TSubclassOf<UGameplayAbility> InAbilityType);
};