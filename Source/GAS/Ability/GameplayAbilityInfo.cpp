// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayAbilityInfo.h"

FGameplayAbilityInfo::FGameplayAbilityInfo()
	: CoolDuration(0.f),
	Cost(0.f),
	CostType(ECostType::ECT_Default),
	IconMat(nullptr),
	AbilityClass(nullptr)
{
	
}

FGameplayAbilityInfo::FGameplayAbilityInfo(float InCoolDuration, float InCost, ECostType InCostType, UMaterialInstance* InIconMat, TSubclassOf<UGameplayAbility> InAbilityType)
	: CoolDuration(InCoolDuration),
	Cost(InCost),
	CostType(InCostType),
	IconMat(InIconMat),
	AbilityClass(InAbilityType)
{
	
}
