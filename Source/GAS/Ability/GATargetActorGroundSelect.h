// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbilityTargetActor.h"
#include "GATargetActorGroundSelect.generated.h"

/**
 * 
 */
UCLASS()
class GAS_API AGATargetActorGroundSelect : public AGameplayAbilityTargetActor
{
	GENERATED_BODY()
public:
	AGATargetActorGroundSelect();
	
	virtual void Tick(float DeltaSeconds) override;
	
	virtual void StartTargeting(UGameplayAbility* Ability) override;

	virtual void ConfirmTargetingAndContinue() override;

	virtual void ConfirmTargeting() override;
	
	UFUNCTION(BlueprintCallable, Category = "GroundSelect")
	bool GetPlayerLookingPoint(FVector& OutViewPoint);

	UPROPERTY(EditAnywhere, BlueprintReadWrite,meta=(ExposeOnSpawn), Category = "GroundSelect")
	float Radius;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Decale", meta=(AllowPrivateAccess=true))
	UDecalComponent* DecalComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "RootComp", meta=(AllowPrivateAccess=true))
	USceneComponent* RootComp;
	
};
