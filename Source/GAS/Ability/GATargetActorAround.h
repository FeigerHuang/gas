// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbilityTargetActor.h"
#include "GATargetActorAround.generated.h"

/**
 * 
 */
UCLASS()
class GAS_API AGATargetActorAround : public AGameplayAbilityTargetActor
{
	GENERATED_BODY()
public:
	AGATargetActorAround();

	virtual void StartTargeting(UGameplayAbility* Ability) override;

	/** Requesting targeting data, but not necessarily stopping/destroying the task. Useful for external target data requests. */
	virtual void ConfirmTargetingAndContinue() override;

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, meta=(ExposeOnSpawn), Category = "Radius" )
	float Radius;
};
